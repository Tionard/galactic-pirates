//Setting speed for keyboard and DPAD
if(keyboard_check(ord('W')) 
or keyboard_check(ord('D')) 
or keyboard_check(ord('A')) 
or keyboard_check(ord('S')) 
or gamepad_button_check(0,gp_padu) 
or gamepad_button_check(0,gp_padd) 
or gamepad_button_check(0,gp_padl) 
or gamepad_button_check(0,gp_padr))
        speed += 1;


//Keyboard basic movement
if(keyboard_check(ord('W'))) direction = 90;
if(keyboard_check(ord('S'))) direction = 270;
if(keyboard_check(ord('A'))) direction = 180;
if(keyboard_check(ord('D'))) direction = 0;
if(keyboard_check(ord('W')) and keyboard_check(ord('A'))) direction = 135;
if(keyboard_check(ord('W')) and keyboard_check(ord('D'))) direction = 45;
if(keyboard_check(ord('S')) and keyboard_check(ord('A'))) direction = 225;
if(keyboard_check(ord('S')) and keyboard_check(ord('D'))) direction = 315;

//Gamepad basic movement
    //DPAD
if gamepad_button_check(0,gp_padu) direction = 90;
if gamepad_button_check(0,gp_padd) direction = 270;
if gamepad_button_check(0,gp_padl) direction = 180;
if gamepad_button_check(0,gp_padr) direction = 0;
if gamepad_button_check(0,gp_padu) and gamepad_button_check(0,gp_padl) direction = 135;
if gamepad_button_check(0,gp_padu) and gamepad_button_check(0,gp_padr) direction = 45;
if gamepad_button_check(0,gp_padd) and gamepad_button_check(0,gp_padl) direction = 225;
if gamepad_button_check(0,gp_padd) and gamepad_button_check(0,gp_padr) direction = 315;

//Left Stick
if gamepad_axis_value(0,gp_axislv)<-0.4 {speed += 1; direction = 90;}
if gamepad_axis_value(0,gp_axislv)>0.4  {speed += 1; direction = 270;}
if gamepad_axis_value(0,gp_axislh)<-0.4 {speed += 1; direction = 180;}
if gamepad_axis_value(0,gp_axislh)>0.4  {speed += 1; direction = 0;}
if gamepad_axis_value(0,gp_axislv)<-0.4 and gamepad_axis_value(0,gp_axislh)<-0.4  {speed += 1; direction = 135;}
if gamepad_axis_value(0,gp_axislv)<-0.4 and gamepad_axis_value(0,gp_axislh)>0.4 {speed += 1; direction = 45;}
if gamepad_axis_value(0,gp_axislv)>0.4 and gamepad_axis_value(0,gp_axislh)<-0.4 {speed += 1; direction = 225;}
if gamepad_axis_value(0,gp_axislv)>0.4 and gamepad_axis_value(0,gp_axislh)>0.4 {speed += 1; direction = 315;}

if(keyboard_check(vk_shift) or gamepad_button_check(0,gp_face1)) //fire
if (alarm[0] =- 1) alarm[0] = fire_counter;

if (rockets>0)
if(keyboard_check_pressed(vk_space) or gamepad_button_check_pressed(0,gp_face3))
{
rocket[1] = instance_create(x,y,obj_player_rocket);
rocket[1].dmg = rocket[1].dmg * rocket_lvl;
rockets-=1;
}

//constant cheking:
if (speed > max_speed) speed=max_speed;

//usable screensize:
x = min(x,min_dir_limit_x);
x = max(x,max_dir_limit_x);
y = min(y,min_dir_limit_y);
y = max(y,max_dir_limit_y );

//death
health=hp;
if(health<=0)
instance_destroy();
