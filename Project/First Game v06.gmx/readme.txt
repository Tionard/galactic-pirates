This is the first game I made in GameMaker Studio 1.4

I wanted to make many different levels and some sort of story, 
but eventually didn't find time to finish the game due to work and study

At current state it's endless game where you can gain score by killing enemies.
There are 4 different types of enemy ships that behave differently.

By killin enemy battleship it has a chance to drop one of three following PowerUps:
- Health Pack (restores some health)
- Upgrade Gun	(adds one extra projectile in different directone. Up to three)
- Extra Rockets (adds 5 Rockets for you to shoot, they are slower 
		then usual projectiles but deal more damage)

Controls:
WASD      - Movement
LeftShift - Normal Fire
Space	  - Rocket'
R         - Restart the room (game)